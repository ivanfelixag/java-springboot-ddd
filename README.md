# Java SpringBoot Specification + DDD
This project bring you the different parts you need to know to start with Java and the Spring Framework + Spring Boot applying Domain Driven Design.

## DDD Directory Tree
![clean_architecture](./clean_architecture.jpg)

Inside the [main folder](./src/main/java/com/concept/ddd/) you will find all the folders that exist in this approach to software development.

In summary, DDD helps decouple our code and make it independent of the data persistence system and framework, allowing us to focus on code and business logic.

If you want to know more about DDD and clean architectures, you can read:  
- [The Big Blue Book of DDD by Eric Evans](https://domainlanguage.com/ddd/)  
- [Uncle Bob's Clean Architectures](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html)

````
.
+-- domain             
|   +-- entities        
|   +-- aggregates      
|   +-- repositories    
|   +-- valueobjects    
+-- application         
|   +-- interfaces      
|   +-- requests        
|   +-- responses       
|   +-- usecases        
+-- infrastructure      
|   +-- configuration   
|   +-- data            
+-- sharedkernel        
|   +-- exceptions      
|   +-- interfaces      
+-- webapi              
|   +-- controllers     
````

As you can see there are 5 main folders:

- **Domain**: Contains all the business objects of the application.  
***entities***: Objects which have an unique identifier in our business logic  
***aggregates***: An aggregate is a group of classes that hold an invariant together. Example: invoice.  
***repositories***: Repository interfaces that are implemented within the infrastructure layer and define the interaction with the persistence system in our business logic.  
***valueobjects***: Concept of the domain model that does not have an identifier, but the element itself is identified by the value of its attributes. Used to describe, measure, or quantify domain concepts

- **Application**: Contains all the business logic of our application.  
***interfaces*** folder contains the use case specifications  
***requests*** folder contains all the inputs of our use cases  
***responses*** folder contains all the outputs of our use cases  
***use cases*** folder contains all the use cases with the business logic


- **Infrastructure**: All of the persistence logic. That includes repository implementations, cache decorators, swagger configuration, the mappers to convert from our domain entities to persistence system entities and vice versa, etc

- **Sharedkernel**: Contains interfaces, business exceptions and other elements that can be shared between one project and another.

- **Webapi**: Contains the different components of the framework, from the outermost layer.


## Spring
Spring is a set of projects that offers us a development framework and simplify the development of our Java solutions.

Some Spring projects you must know:

- Spring Framework and the core AOP
- Spring Boot
- Spring Data
- Spring Batch
- Spring WebFlux
- Spring Cloud

Link: https://docs.spring.io/

## Maven
Maven is a tool that standardizes the configuration of a project throughout its life cycle, for example in all the phases of compilation and packaging and the installation of library distribution mechanisms, so that they can be used by other developers and teams of developing.

Maven also contemplates topics related to continuous integration, to be able to perform unit tests and automated tests, integration tests, etc

Link: https://maven.apache.org/

### Where are the dependencies?
Maven allows us to manage dependencies in the [**pom.xml**](./pom.xml) file located in the root folder.

## Libraries and Tools
There are different libraries that have to be known to make development in Java more enjoyable. The recommended libraries are:

- **Lombok**: It is a library that allows us to reduce the number of lines that we have to program avoiding writing the setters, getters, constructors without parameters or constructors with all the parameters.  
Example: [@Getter, @Setter, @NoArgsConstructor](./src/main/java/com/concept/ddd/domain/entities/User.java)  
Link: https://projectlombok.org/

- **Jackson**: It is a java library that allows us to convert classes to JSON text and vice versa.  
Link: https://github.com/FasterXML/jackson

- **Mapstruct**: It is a code generator that greatly simplifies the implementation of mappings between Objects, DTOs or any class.  
Example: [@Mapper](./src/main/java/com/concept/ddd/infrastructure/data/mappers/UserMapper.java)  
Link: https://mapstruct.org/

- **Springfox-swagger**: It is the library that allows us to configure Swagger and Swagger UI for our API.  
Link: https://springfox.github.io/springfox/

- **Cucumber**: It is a tool we can use to automate our tests in BDD. In addition, it will allow us to use functional descriptions in plain text, such as automated software tests.  
Link: https://cucumber.io/

- **JUnit**: It is a tool that will allow us to develop and run our unit tests.  
Link: https://junit.org/

- **Mockito**: It is a mock framework which can be used in conjunction with JUnit which allows us to create and configure mock objects.  
Link: https://site.mockito.org/

## Swagger Configuration
To configure Swagger you need to follow 2 steps:

1. Add the dependencies in the [**pom.xml**](./pom.xml) file:
````
...
<properties>
		<org.springfox-swagger.version>2.9.2</org.springfox-swagger.version>
</properties>
....
<dependencies>
    ...
    <dependency>
        <groupId>io.springfox</groupId>
        <artifactId>springfox-swagger2</artifactId>
        <version>${org.springfox-swagger.version}</version>
    </dependency>
    <dependency>
        <groupId>io.springfox</groupId>
        <artifactId>springfox-swagger-ui</artifactId>
        <version>${org.springfox-swagger.version}</version>
    </dependency>
    <dependency>
        <groupId>io.springfox</groupId>
        <artifactId>springfox-bean-validators</artifactId>
        <version>${org.springfox-swagger.version}</version>
    </dependency>
    ...
</dependencies>
...
````
2. Create a class [SwaggerConfiguration](./src/main/java/com/concept/ddd/infrastructure/configuration/SwaggerConfiguration.java) that contains the @Bean that will allow SpringFox Swagger to configure our API documentation.

## Internationalization vs Error codes
When we develop an API it is better to think about error codes instead of internationalizing the error messages, this will allow us to decouple this logic in the frontend.

For this reason, our API must have available services that allow the developer to know all the errors that may occur.

## Local Docker Compose

Although Packer will be used to create the image of our solution, for development in Local it is easier to use Docker Compose, since it allows us to start all the pieces, such as the database, that we need for testing and development.

The **docker-compose.yml** file will be located in the [root folder](./) of the project

## CI / CD
As part of the development team, you need to know the concepts surrounding continuous integration, delivery and deployments.

Before starting the project, you need to request the Architecture / DevSecOps team to provide you with 3 elements:

1. **azure-pipelines.yml**: This file contains the MultiBranch and MultiStage Azure pipeline for Java and the type of cloud service like: IaaS, PaaS, CaaS,...
2. **pipeline_resources folder**: This folder contains everything the pipeline needs to be executed. It also contains the configuration as code, *Ansible*, and the image as code, *Packer*.

**Both resources will be located in the [root folder](./) of the project**

## SonarQube for Code Quality
On the other hand, you need to configure the Sonar Scanner for Maven which will be used for analysis during continuous integration.

Link: https://docs.sonarqube.org/latest/analysis/scan/sonarscanner-for-maven/

## Testing Configuration and Test folder
The first step we have to do is add the dependencies to the [**pom.xml**](./pom.xml) file (the version can change):

For **Unit Testing**: JUnit + Mockito
````
<dependency>
   <groupId>junit</groupId>
   <artifactId>junit</artifactId>
   <version>4.12</version>
   <scope>test</scope>
</dependency>
<dependency>
	<groupId>org.mockito</groupId>
	<artifactId>mockito-all</artifactId>
	<version>1.9.5</version>
	<scope>test</scope>
</dependency>
````

For **BDD**: Cucumber
````
<dependency>
   <groupId>io.cucumber</groupId>
   <artifactId>cucumber-java</artifactId>
   <version>4.2.0</version>
   <scope>test</scope>
</dependency>
<dependency>
   <groupId>io.cucumber</groupId>
   <artifactId>cucumber-junit</artifactId>
   <version>4.2.0</version>
   <scope>test</scope>
</dependency>
<dependency>
   <groupId>io.cucumber</groupId>
   <artifactId>cucumber-expressions</artifactId>
   <version>6.2.0</version>
   <scope>test</scope>
</dependency>
````

You must to create your tests in the [**tests**](./src/test/java/com/concept/ddd) folder.  
Example JUnit + Mockito: [UserTests.java](./src/test/java/com/concept/ddd/UserTests.java)