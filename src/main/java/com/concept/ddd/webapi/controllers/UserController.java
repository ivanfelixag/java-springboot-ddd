package com.concept.ddd.webapi.controllers;

import com.concept.ddd.application.interfaces.IGetUser;
import com.concept.ddd.application.requests.GetUserRequest;
import com.concept.ddd.application.responses.GetUserResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private IGetUser getUserUseCase;

    @GetMapping("/name")
    GetUserResponse getUser(@RequestParam(value = "name", defaultValue = "Test") String name) {
        return getUserUseCase.execute(new GetUserRequest(name));
    }

}