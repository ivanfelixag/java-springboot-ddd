package com.concept.ddd.infrastructure.data.repositories;

import java.util.Optional;
import java.util.UUID;

import com.concept.ddd.domain.entities.User;
import com.concept.ddd.domain.repositories.IUserRepository;
import com.concept.ddd.infrastructure.data.entities.UserEntity;
import com.concept.ddd.infrastructure.data.mappers.UserMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserRepository implements IUserRepository {

    @Autowired
    private SpringDataUserRepository userRepository;

    @Autowired
    private UserMapper userMapper;

    public Optional<User> findByName(String name) {
        Optional<UserEntity> userEntity = userRepository.findByName(name);
        if (userEntity.isPresent()) {
            return Optional.of(userMapper.toDomain(userEntity.get()));
        } else {
            return Optional.empty();
        }
    }

    @Override
    public Optional<User> findById(UUID id) {
        Optional<UserEntity> userEntity = userRepository.findById(id);
        if (userEntity.isPresent()) {
            return Optional.of(userMapper.toDomain(userEntity.get()));
        } else {
            return Optional.empty();
        }
    }

    @Override
    public User save(User entity) {
        UserEntity user = userRepository.save(userMapper.toDbEntity(entity));
        return userMapper.toDomain(user);
    }
    
}