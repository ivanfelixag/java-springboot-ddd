package com.concept.ddd.infrastructure.data.repositories;

import java.util.Optional;
import java.util.UUID;

import com.concept.ddd.infrastructure.data.entities.UserEntity;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface SpringDataUserRepository extends CrudRepository<UserEntity, UUID> {
    
    @Query("SELECT u FROM UserEntity u WHERE u.name = :name")
    Optional<UserEntity> findByName(@Param("name") String name);

}