package com.concept.ddd.infrastructure.data.mappers;

import com.concept.ddd.domain.entities.User;
import com.concept.ddd.infrastructure.data.entities.UserEntity;

import org.mapstruct.Mapper;

@Mapper(componentModel = "spring") // Spring IoC Configuration
public interface UserMapper {
    User toDomain(UserEntity entity);

    UserEntity toDbEntity(User domain);
}