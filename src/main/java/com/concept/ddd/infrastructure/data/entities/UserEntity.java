package com.concept.ddd.infrastructure.data.entities;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.concept.ddd.domain.entities.User;
import com.concept.ddd.domain.valueobjects.UserStatus;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "users")
public @NoArgsConstructor @Getter @Setter class UserEntity {
    
    @Id
    private UUID id;

    @NotNull
    @Column(name = "status", nullable = false)
    private UserStatus status;

    @NotNull
    @NotEmpty
    @Column(name = "name", nullable = false, unique = true)
    private String name;
    
}