package com.concept.ddd.infrastructure.configuration;

import com.concept.ddd.DddApplication;
import com.concept.ddd.application.interfaces.*;
import com.concept.ddd.application.usecases.*;
import com.concept.ddd.domain.repositories.*;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackageClasses = DddApplication.class)
public class BeanConfiguration {
    
    @Bean
    IGetUser getUser(final IUserRepository userRepository) {
        return new GetUser(userRepository);
    }

}