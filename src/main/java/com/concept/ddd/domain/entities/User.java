package com.concept.ddd.domain.entities;

import java.util.UUID;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.concept.ddd.domain.valueobjects.UserStatus;
import com.concept.ddd.sharedkernel.exceptions.DomainException;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class User {
    @NotNull
    private UUID id;

    @NotNull
    private UserStatus status = UserStatus.PENDING_CONFIRM;

    @NotNull
    @Size(min = 1, max = 20)
    private String name;

    public void markCreated() {
        this.validateStatus(UserStatus.PENDING_CONFIRM);
        this.status = UserStatus.CREATED;
    }

    private void validateStatus(UserStatus status) {
        if (UserStatus.CREATED.equals(status)) {
            throw new DomainException("The order is in completed state.");
        }
    }
}