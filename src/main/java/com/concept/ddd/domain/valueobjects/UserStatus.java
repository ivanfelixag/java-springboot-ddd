package com.concept.ddd.domain.valueobjects;

public enum UserStatus {
    CREATED, PENDING_CONFIRM
}