package com.concept.ddd.domain.repositories;

import java.util.Optional;
import java.util.UUID;

import com.concept.ddd.domain.entities.User;
import com.concept.ddd.sharedkernel.interfaces.IRepository;

public interface IUserRepository extends IRepository<User, UUID> {
    
    Optional<User> findByName(String name);

}