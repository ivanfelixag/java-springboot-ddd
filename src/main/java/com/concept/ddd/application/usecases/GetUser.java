package com.concept.ddd.application.usecases;

import java.util.Optional;

import com.concept.ddd.application.interfaces.IGetUser;
import com.concept.ddd.application.requests.GetUserRequest;
import com.concept.ddd.application.responses.GetUserResponse;
import com.concept.ddd.domain.entities.User;
import com.concept.ddd.sharedkernel.exceptions.NotFoundException;
import com.concept.ddd.domain.repositories.IUserRepository;

public class GetUser implements IGetUser {

    private final IUserRepository userRepository;

    public GetUser(final IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public GetUserResponse execute(GetUserRequest request) {
        String name = request.getName();

        Optional<User> user = this.userRepository.findByName(name);

        if(!user.isPresent()) {
            throw new NotFoundException(String.format("User with name %s not found", name));
        }

        return new GetUserResponse(user.get());
    }
    
}