package com.concept.ddd.application.interfaces;

import com.concept.ddd.application.requests.GetUserRequest;
import com.concept.ddd.application.responses.GetUserResponse;
import com.concept.ddd.sharedkernel.interfaces.IUseCase;

public interface IGetUser extends IUseCase<GetUserRequest, GetUserResponse> {
    
}