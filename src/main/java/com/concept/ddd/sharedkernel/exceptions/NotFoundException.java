package com.concept.ddd.sharedkernel.exceptions;

public class NotFoundException extends DomainException{

    public NotFoundException(String message) {
        super(message);
    }
    
}