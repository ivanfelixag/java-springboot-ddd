package com.concept.ddd.sharedkernel.exceptions;

public class DomainException extends RuntimeException {

    public DomainException(final String message) {
        super(message);
    }
}