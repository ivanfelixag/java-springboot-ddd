package com.concept.ddd.sharedkernel.interfaces;

import java.util.Optional;

public interface IRepository<T, ID> {

    Optional<T> findById(ID id);
    T save(T entity);
    
}