package com.concept.ddd.sharedkernel.interfaces;

public interface IUseCase<UseCaseRequest, UseCaseResponse> {
    UseCaseResponse execute(UseCaseRequest request);
}