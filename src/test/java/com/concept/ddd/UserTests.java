package com.concept.ddd;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.concept.ddd.domain.entities.User;
import com.concept.ddd.domain.repositories.IUserRepository;
import com.concept.ddd.application.requests.GetUserRequest;
import com.concept.ddd.application.responses.GetUserResponse;
import com.concept.ddd.application.usecases.GetUser;
import com.concept.ddd.domain.valueobjects.UserStatus;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.Mockito.when;

import java.util.Optional;

public class UserTests {

    @InjectMocks
    GetUser getUserUseCase;
    
    @Mock
    IUserRepository userRepository;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void GetUserByName()
    {
        String nameToFind = "testUser";

        User user = new User();
        user.setName(nameToFind);
        user.setStatus(UserStatus.CREATED);

        Optional<User> optionalUser = Optional.of(user);

        when(userRepository.findByName(nameToFind)).thenReturn(optionalUser);

        GetUserResponse response = getUserUseCase.execute(new GetUserRequest(nameToFind));

        assertEquals(nameToFind, response.getUser().getName());
    }

}